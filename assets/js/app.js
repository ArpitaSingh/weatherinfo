var app = angular.module('app', []);

app.controller('weatherCtrl', ["$scope", "$http", function($scope, $http){

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        $scope.autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
            {types: ['(cities)']});

        // When the user selects an address from the dropdown, called an function
        google.maps.event.addListener($scope.autocomplete, 'place_changed', function(){
            $scope.place = $scope.autocomplete.getPlace();

            hideErrorMessage();
            $http.get("http://api.openweathermap.org/data/2.5/weather?q="+$scope.place.name+"&APPID=c11d1e0e21ea035befad19cb13bc1a3a&units=metric",
            {timeout: 10000}) //timeout of 10 seconds
                .then(function(response){
                  // console.log(response);
                  processWeatherData(response.data);
                },
                function(err){
                    if(err.status == -1){
                      showErrorMessage("Request is taking more time than expected. <br/> Please try again!");
                    }else{
                      showErrorMessage("Sorry! City not found.");
                    }
              });
         });
    }

    function showErrorMessage(mesg){
      $(".errormesg").html(mesg).show();
      $(".wrapper").hide();
    }

    function hideErrorMessage(){
      $(".errormesg").hide();
      $(".wrapper").show();
    }

    function processWeatherData(data){
      $scope.weatherData = data;
      $scope.darray= {};
      $scope.darray['City']= $scope.weatherData.name;
      $scope.darray['Sky']= $scope.weatherData.weather[0].main;
      $scope.darray['Temp']= Math.floor($scope.weatherData.main.temp) +" °C";
      $scope.darray['Min Temperature']= Math.floor($scope.weatherData.main.temp_min)+" °C";
      $scope.darray['Max Temperature']= Math.floor($scope.weatherData.main.temp_max) +" °C";
      $scope.darray['Wind Speed']=$scope.weatherData.wind.speed+ " KM/H";
      $scope.darray['Sunrise']= time($scope.weatherData.sys.sunrise);
      $scope.darray['Sunset']= time($scope.weatherData.sys.sunset);
      //console.log($scope.darray);
    }

    function time(s) {
      return new Date(s * 1000).toLocaleTimeString();
    }

    initAutocomplete();

}]);
